# Workshop Biometadata-02

Companion repository for the [Workshop: How to describe biological data? A primer to a FAIR approach for now and the future](https://biometadata-02.sciencesconf.org/).

The slides for the workshop are rendered as a website via GitLab Pages at: <https://clavellab.pages.rwth-aachen.de/workshop-biometadata-02>

## Usage for developers and contributors

1. Clone the repository on your local computer (see [the Gitlab manual for help](https://docs.gitlab.com/ee/topics/git/clone.html))
2. Open the `workshop-biometadata-02.Rproj` RStudio projcet file with [RStudio](https://posit.co/download/rstudio-desktop/)
3. Open `index.qmd` with RStudio and click on the "Render" button in RStudio to trigger the compilation of the workshop website using [Quarto](https://quarto.org/docs/get-started/hello/rstudio.html)
4. Alternatively, open any one of the lectures `.qmd` files in `lectures/` with RStudio and click on the "Render" button in RStudio to trigger the compilation of the lecture slides using [Quarto](https://quarto.org/docs/get-started/hello/rstudio.html)
5. Find more information about the Quarto syntax for the slides in the [Quarto Presentations with Reveal.js User guide](https://quarto.org/docs/presentations/revealjs/).

## License

All materials on this repository are distributed under the [CC-BY-4.0 license](LICENSE).

You can cite the workshop as follow:

> Pauvert, C., & Magel, M. (2024, June 7). Workshop Biometadata-02: How to describe biological data? A primer to a FAIR approach for now and the future. Zenodo. <https://doi.org/10.5281/zenodo.11527597>

## Funding

This workshop was made possible thanks to the support of the [NFDI4Microbiota](https://nfdi4microbiota.de), a consortium funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) — 460129525, that is part of the German NFDI (National research Data Infrastructure). One of the missions of the NFDI4Microbiota is to support the microbiology research community in making its data more FAIR via adequate tools and training.
