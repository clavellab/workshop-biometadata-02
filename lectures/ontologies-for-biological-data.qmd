---
title: "Ontologies for biological data"
author:
  - name: Maja Magel
  - name: Charlie Pauvert
date: "2024-06-07"
format:
  revealjs: default
categories: [Lecture, Ontology]
order: 4
---

## Learning objectives

-   **define** an ontology and **explain** its features
-   **name** at least two widely used ontologies in the life sciences
- **navigate** an ontology service and **find** specific ontology terms 

<!--# Should we demonstrate how to use the hierarchy feature of OLS? -->

## Why bother with ontologies?
::: incremental
- increase findability of your dataset
- improve machine-readability of your datasets
- help others correctly categorize & re-use your datasets > [recontextualization]{.orange}
- required by data repositories
- first step towards open linked data and knowledge graph representations? 
:::

## Having fun with ontologies 
What is a `pie`? 

::: {.fragment .fade-up}
Where you thinking of the baked goods, such as `cake [ENVO:02000063]`?
:::
<!--# include pie picture -->

::: {.fragment .fade-up}
> unfortunately there was no well described ontology term for `pie` and I've used cake instead :D 
:::



::: {.fragment .fade-up}
Or were you thinking of the `pie [44315]` gene that encodes the `pineapple eye protein (fruit fly) [PR:Q9VKW2]`?
:::

<!--# include fruitfly picture -->





::: notes
- add picture of a blueberry pie or blueberry cake?
- blueberry pie hyperlink to picture: 
- blue berry pie picture attribution: [Kelly Sue DeConnick](https://flickr.com/people/44124395312@N01) - originally posted to [Flickr](https://commons.wikimedia.org/wiki/Flickr) as [Blueberry Pie with Almond Crumb Topping](https://flickr.com/photos/44124395312@N01/3570328175) licensed under [Creative Commons Attribution-Share Alike 2.0 Generic](https://en.wikipedia.org/wiki/en:Creative_Commons)

fruit fly picture attribution:
[Drosophila melanogaster feeding on banana](https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Drosophila_melanogaster_Proboscis.jpg/1200px-Drosophila_melanogaster_Proboscis.jpg?20171104225100) by [Sanjay Acharya](https://commons.wikimedia.org/wiki/User:Sanjay_ach) licensed under [Creative Commons Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) 

:::

## Ontology definition {.smaller}

::: incremental
-   List of terms, usually taken from the scientific literature

-   Ontology terms:

    -   have curated **textual definitions** and synonyms

    -   are arranged in a **hierarchy** from general to specific

    -   have defined **relationships** with others terms (e.g., `is_a`, `has_condition`)

    -   have persistent **identifiers**

    -   can be **cross-referenced** with other resources (ontology or not)

-   Ontology should reflect existing knowledge
:::

::: aside
Adapted from @osumi-sutherland_introduction_2023 and @leonelli_data-centric_2016 [ pp. 125, 133]
:::

## Exercise: an ontology?

**Task**: Choose one of the definitions below and, if necessary add any *missing features*.

::: {.fragment .fade-up}
An [ontology]{.orange} is:

-   A dictionary with persistent identifiers

-   A network of scientific definitions

-   A controlled vocabulary with synonyms
:::

::: notes
Formative assessment of ontology understanding

-   A dictionary with persistent identifiers (missing hierarchy of terms, [relationships])

-   A network of scientific definitions (missing curation and missing identifier)

-   A controlled vocabulary with synonyms (missing definitions, persistent identifier and missing hierarchy)

Cross references as well
:::

# Search ontology terms


## Search terms in ontologies {.smaller}

You can find terms in ontologies using the search bar of:

-   [BioPortal](https://bioportal.bioontology.org/)

-   [Ontology Lookup Service v4](https://www.ebi.ac.uk/ols4)




::: r-stack
![](images/OntologyLookupService_search.png){.fragment fig-alt="OLS search screenshot" fig-align="center"}

![](images/NCBOBioPortal_search.png){.fragment fig-alt="NCBO BioPortal search screenshot" fig-align="center"}
:::

::: {.fragment .fade-up}
- others: [The OBO Foundry](http://obofoundry.org), [OntoBee](https://ontobee.org) mainly for developers
:::

## Ontology Lookup Service

> OLS is the official ontology service of the EMBL-EBI
>
> <https://www.ebi.ac.uk/ols4>

-   EMBL: European Molecular Biology Laboratory
-   EMBL-EBI: EMBL's European Bioinformatics Institute

::: {.fragment .fade-up}
>Demonstration: navigating OLS for term "intestines" in UBERON, ENVO
:::

::: notes
- search intestines
- restrict search to ontologies "UBERON, ENVO"
- point out that first hit /= from requested ontology
- exact match > term not showing up
- correct spelling to "Intestine"
- explain ontology vs. present in and "imported"
demonstration:
- point out: term identifier & copy function 
- definition & synonyms
- term relations / class relations
- persistent identifier
:::

## Ontology definition as demonstrated with the term "intestine" {.smaller}
::: {.fragment .fade-up}
Results, we found the term
:::

::: {.fragment .fade-up}
**intestine [UBERON:0000160]**
:::

::: {.fragment .fade-up}
in both ontologies UBERON and ENVO (as imported term from UBERON)
:::

::: {.fragment .fade-up}
> The correct writing convention for ontology terms and their term identifiers is: 
> > term [ontology-acronym:sequence-number], e.g. intestine [UBERON:0000160]
:::


<!--# include picture of UBERON:0000160 intestine and mark with boxes each element corresponding to the ontology definition especially for term identifier--> 


## Exercise: Ontology browser {.smaller}

::: columns
::: {.column width="50%"}
**Task**: using [Ontology Lookup Service v4](https://www.ebi.ac.uk/ols4)

::: incremental
-   **Look-up** the following keywords in that order via the search bar*:* [pond]{.orange}, [ear]{.orange} and [leaf]{.orange}

-   **Select** a term for each using these ontologies:

    -   Uber-anatomy ontology (UBERON)

    -   Plant Ontology (PO)

    -   Environmental Ontology (ENVO)

-   **Report** the term and the term identifier in the pad
:::
:::

::: {.column width="50%"}
![](images/Auguste_Renoir_-_La_Grenouillere.jpg){.fragment fragment-index="1" fig-alt="painting of a pond la grenouillere by auguste renoir" fig-align="center"}
:::
:::

::: aside
*La Grenouillère* by Auguste Renoir (1869) [Wikimedia](https://commons.wikimedia.org/wiki/File:Auguste_Renoir_-_La_Grenouill%C3%A8re.jpg)
:::

::: notes
-   [pond \[ENVO:00000033\]](https://www.ebi.ac.uk/ols4/ontologies/envo/classes/http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252FENVO_00000033) (the correct term is not far down the list)

-   [ear \[UBERON:0001690\]](https://www.ebi.ac.uk/ols4/ontologies/uberon/classes/http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252FUBERON_0001690) (many "early" that can be filtered with the exact match option)

-   [leaf \[PO:0025034\]](https://www.ebi.ac.uk/ols4/ontologies/po/classes/http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252FPO_0025034) (tricky because it is an imported term)
:::

## Exercise: visualize features

::: incremental
-   **Navigate** to 1-2 of the following terms:

    -   [pond \[ENVO:00000033\]](https://www.ebi.ac.uk/ols4/ontologies/envo/classes/http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252FENVO_00000033)

    -   [ear \[UBERON:0001690\]](https://www.ebi.ac.uk/ols4/ontologies/uberon/classes/http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252FUBERON_0001690)

    -   [leaf \[PO:0025034\]](https://www.ebi.ac.uk/ols4/ontologies/po/classes/http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252FPO_0025034)

-   **Inspect** the term page

-   **Associate** parts of the OLS interface with the [expected features of an ontology](https://clavellab.pages.rwth-aachen.de/workshop-biometadata-02/lectures/ontologies-for-biological-data.html#/ontology-definition)
- [**Let's test**]{.green} your understanding of the features [team vs. team]{.orange}
:::

::: notes
Restitution can be made with everyone closer to the screen displaying one of the terms and pointing to each of the ontology features.
**OR:** Gamify the association features:
the lecturer points to a feature on the page and the teams have to quickly press a "Buzzer/knock on the table" and then answer. 1 point to the correct team.
continue until all features have been named: 

- curated **textual definitions**
- synonyms
- **hierarchy** from general to specific
- **relationships** with others terms (e.g., `is_a`, `has_condition`)
- persistent **identifiers**
-  **cross-referenced** with other resources (ontology or not)
:::


## What questions do you have?
::: {.fragment .fade-up}
Learning objectives achieved?
:::
::: incremental
-   **define** an ontology and **explain** its features
- **navigate** an ontology service and **find** specific ontology terms 
-   **name** at least two widely used ontologies in the life sciences
:::


## Exercise: Find your ontology terms & connect the dots {.smaller}
::: {.fragment .fade-up}
Can we start the next lesson on selecting ontology terms for **YOUR** dataset descriptions?
:::

::: {.fragment .fade-up}
**TASK**: Pick any 3 terms from your dataset description.
:::
::: incremental
- **Note** them in the pad.
- Browse the OLS and **find 1-2 suitable ontology terms for each**.
- **Add the ontology term identifiers** to the pad.
- [You have 5 minutes.]{.green}
- Share your experiences.
- Who used an ontology besides UBERON, ENVO and PO? And why?
:::

## Exercise: Find your ontology terms & connect the dots 2{.smaller}

**Connect the dots to repository metadata fields**

:::incremental
- Select a suitable [ENA checklist](https://www.ebi.ac.uk/ena/browser/checklists) for your dataset and match your terms to one of the metadata fields.
- Write down the checklist identifier & metadata field. 
- Are your terms fitting the metadata field requirements (field format, restriction)?
- Does the info box tell you to use specific ontologies?
- [10 more minutes]{.green}
:::


## Environmental metadata according to established metadata standards{.smaller}

MIxS lists three *mandatory* environmental metadata fields that expect [ontology]{.orange} terms.

::: r-stack
::: {.fragment .fade-in-then-out}
| Metadata field                    | Abbreviation      | Definition |
|----------------------------------|-------------------|---------------------------------|
| broad-scale environmental context | `env_broad_scale` | global correlation; ecosystem | 
| local environmental context       | `env_local_scale` |in local vicinity; causal influences | 
| environmental medium              | `env_medium`      | immediate surroundings of your sample during sampling |
:::

::: fragment
| Metadata field                    | Abbreviation      | Recommended use of subclasses from |
|-------------------------------|------------------|------------------------------|
| broad-scale environmental context | `env_broad_scale` | [biome \[ENVO:00000428\]](https://www.ebi.ac.uk/ols4/ontologies/envo/classes/http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252FENVO_00000428)                  |
| local environmental context       | `env_local_scale` | deeper hierarchy than broad-scale (UBERON terms accepted)                                                                                                       |
| environmental medium              | `env_medium`      | [environmental material \[ENVO:00010483\]](https://www.ebi.ac.uk/ols4/ontologies/envo/classes/http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252FENVO_00010483) |
:::
:::

## Exercise: Env\* metadata {.smaller}

::: columns
::: {.column width="50%"}
![](images/meme-environmental-context.jpg){.fragment fig-alt="broad scale vs local env context" fig-align="center" width="340"}
:::

::: {.column .fragment width="50%"}
**Task alone or by pairs**:

-   **Browse** ENVO or UBERON (see previous table)

-   **List** ontology terms fitting your data

-   **Fill out** the following template on the pad:

    -   `env_broad_scale` 

    -   `env_local_scale`

    -   `env_medium`
    
- trouble finding an appropriate term downstream of the recommended class? See [instructions of using other ontologies with the MIxS standard](https://github.com/EnvironmentOntology/envo/wiki/Using-ENVO-with-MIxS#notes-on-the-use-of-other-ontologies-in-mixs-environment-fields)
:::
:::


::: aside
Get more help on [ENVO's wiki page about microbial samples](https://github.com/EnvironmentOntology/envo/wiki/Using-ENVO-with-MIxS#notes-on-annotating-microscale-or-microbial-samples)!
:::


:::notes
- Depending on the time limit: Execute Deep dive task OR ask if anyone thinks this is an efficient way of collecting & adapting your metadata?
- Goal: 

check the transition to the next lesson, include small exercise or table of stating 3 terms and identifying an ontology term (free-style)
- allow to use any ontology
- 
<!--# include picture of UBERON:0000160 intestine and mark with boxes each element corresponding to the ontology definition especially for term identifier-->
:::

# References
